<?php
require_once(dirname(dirname(__FILE__)) . '/app.php');

need_login();

$id = abs(intval($_GET['id']));
$team = Table::Fetch('team', $id);
if ( !$team || $team['begin_time']>time() ) {
	Session::Set('error', '该项目不存在');
	redirect( WEB_ROOT . '/index.php' );
}

//whether buy
$ex_con = array(
		'user_id' => $login_user_id,
		'team_id' => $team['id'],
		);
$order = DB::LimitQuery('recom_vote', array(
	'condition' => $ex_con,
	'one' => true,
));

//buyonce
if (strtoupper($team['buyonce'])=='Y') {
	if ( Table::Count('recom_vote', $ex_con) ) {
		Session::Set('error', '您已经成功投过票了，请勿重复，快去关注一下其他产品吧！');
		redirect( WEB_ROOT . "/team.php?id={$id}"); 
	}
}

//peruser buy count
if ($team['per_number']>0) {
	$now_count = Table::Count('recom_vote', array(
		'user_id' => $login_user_id,
		'team_id' => $id,
	), 'quantity');
	$team['per_number'] -= $now_count;
	if ($team['per_number']<=0) {
		Session::Set('error', '您投票次数已经达到上限，快去关注一下其他产品吧！');
		redirect( WEB_ROOT . "/team.php?id={$id}"); 
	}
}

$values=array('user_id'=>$login_user_id,'team_id'=>$team['id']);
if(DB::Insert('recom_vote',$values) && DB::Update('team',$team['id'],array('now_number'=>$team['now_number']+1))){
	Session::Set('notice', '投票成功');
}
else Session::Set('error', '出错了！');
redirect(WEB_ROOT . "/team.php?id={$id}");