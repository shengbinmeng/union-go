<?php include template("header");?>
<?php if(is_get()){?>
<div class="sysmsgw" id="sysmsg-error"><div class="sysmsg"><p>此订单尚未完成付款，请重新付款</p><span class="close">关闭</span></div></div>
<?php }?>

<div id="bdw" class="bdw">
<div id="bd" class="cf">
<div id="order-pay">
    <div id="content">
        <div id="deal-buy" class="box">
            <div class="box-top"></div>
            <div class="box-content">
                <div class="head">
                    <h2>应付总额：<strong class="total-money"><?php echo moneyit($total_money); ?></strong> 元</h2>
                </div>
                <div class="sect">
                    <div style="text-align:left;">
<?php if($order['service']=='credit'){?>
<form id="order-pay-credit-form" method="post" sid="<?php echo $order_id; ?>">
	<input type="hidden" name="order_id" value="<?php echo $order_id; ?>" />
	<input type="hidden" name="service" value="credit" />
	<input type="submit" class="formbutton gotopay" value="使用账户余额支付" />
</form>
<?php } else if($order['service']=='tenpay') { ?>
<form id="order-pay-form" method="post" sid="<?php echo $order_id; ?>" target="_blank">
	<input type="hidden" name="order_id" value="<?php echo $order_id; ?>" />
	<input type="hidden" name="reqUrl" value="<?php echo $reqUrl; ?>" />
	<input type="hidden" name="action" value="redirect" />
	<img src="/static/css/i/tenpay.jpg" /><br/>
	<input type="submit" class="formbutton" value="前往财付通付款" />
</form>
<?php } else if($order['service']=='alipay') { ?>
<form id="order-pay-form" method="post" action="https://www.alipay.com/cooperate/gateway.do?_input_charset=<?php echo $_input_charset; ?>" target="_blank" sid="<?php echo $order['id']; ?>">
	<input type="hidden" name="body" value="<?php echo $body; ?>" />
	<input type="hidden" name="notify_url" value="<?php echo $notify_url; ?>" />
	<input type="hidden" name="out_trade_no" value="<?php echo $out_trade_no; ?>" />
	<input type="hidden" name="partner" value="<?php echo $partner; ?>" />
	<input type="hidden" name="payment_type" value="1" />
	<input type="hidden" name="return_url" value="<?php echo $return_url; ?>" />
	<input type="hidden" name="seller_email" value="<?php echo $seller_email; ?>" />
	<input type="hidden" name="service" value="<?php echo $service; ?>" />
	<input type="hidden" name="show_url" value="<?php echo $show_url; ?>" />
	<input type="hidden" name="subject" value="<?php echo $subject; ?>" />
	<input type="hidden" name="total_fee" value="<?php echo $total_money; ?>" />
<?php if($itbpay){?><input type="hidden" name="it_b_pay" value="<?php echo $itbpay; ?>" /><?php }?>
	<input type="hidden" name="sign_type" value="<?php echo $sign_type; ?>" />
	<input type="hidden" name="sign" value="<?php echo $sign; ?>" />
	<img src="/static/css/i/alipay.png" /><br /><input type="submit" class="formbutton gotopay" value="前往支付宝支付" />
</form>
<?php } else if($order['service']=='bill') { ?>
<form id="order-pay-form"  target="_blank" method="post" action="https://www.99bill.com/gateway/recvMerchantInfoAction.htm" sid="<?php echo $order['id']; ?>">
	<input type="hidden" name="inputCharset" value="<?php echo $inputCharset; ?>"/>
	<input type="hidden" name="bgUrl" value="<?php echo $bgUrl; ?>"/>
	<input type="hidden" name="pageUrl" value="<?php echo $pageUrl; ?>"/>
	<input type="hidden" name="version" value="<?php echo $version; ?>"/>
	<input type="hidden" name="language" value="<?php echo $language; ?>"/>
	<input type="hidden" name="signType" value="<?php echo $signType; ?>"/>
	<input type="hidden" name="signMsg" value="<?php echo $signMsg; ?>"/>
	<input type="hidden" name="merchantAcctId" value="<?php echo $merchantAcctId; ?>"/>
	<input type="hidden" name="payerName" value="<?php echo $payerName; ?>"/>
	<input type="hidden" name="payerContactType" value="<?php echo $payerContactType; ?>"/>
	<input type="hidden" name="payerContact" value="<?php echo $payerContact; ?>"/>
	<input type="hidden" name="orderId" value="<?php echo $orderId; ?>"/>
	<input type="hidden" name="orderAmount" value="<?php echo $orderAmount; ?>"/>
	<input type="hidden" name="orderTime" value="<?php echo $orderTime; ?>"/>
	<input type="hidden" name="productName" value="<?php echo $productName; ?>"/>
	<input type="hidden" name="productNum" value="<?php echo $productNum; ?>"/>
	<input type="hidden" name="productId" value="<?php echo $productId; ?>"/>
	<input type="hidden" name="productDesc" value="<?php echo $productDesc; ?>"/>
	<input type="hidden" name="ext1" value="<?php echo $ext1; ?>"/>
	<input type="hidden" name="ext2" value="<?php echo $ext2; ?>"/>
	<input type="hidden" name="payType" value="<?php echo $payType; ?>"/>
	<input type="hidden" name="bankId" value="<?php echo $bankId; ?>"/>
	<input type="hidden" name="redoFlag" value="<?php echo $redoFlag; ?>"/>
	<input type="hidden" name="pid" value="<?php echo $pid; ?>"/>
	<img src="/static/css/i/99bill.png" /><br /><input type="submit" class="formbutton gotopay" value="前往快钱支付" />
</form>
<?php } else if($order['service']=='paypal') { ?>
<form id="order-pay-form" method="post" action="<?php echo $post_url; ?>" target="_blank" sid="<?php echo $order['id']; ?>">
	<!-- PayPal Configuration -->
	<input type="hidden" name="business" value="<?php echo $business; ?>">
	<input type="hidden" name="cmd" value="<?php echo $cmd; ?>">
	<input type="hidden" name="return" value="<?php echo $return_url; ?>">
	<input type="hidden" name="cancel_return" value="<?php echo $cancel_url; ?>">
	<input type="hidden" name="notify_url" value="<?php echo $notify_url; ?>">
	<input type="hidden" name="rm" value="2">
	<input type="hidden" name="currency_code" value="<?php echo $currency_code; ?>">
	<input type="hidden" name="lc" value="<?php echo $location; ?>">
	<input type="hidden" name="charset" value="utf-8" />

	<!-- Payment Page Information -->
	<input type="hidden" name="no_shipping" value="1">
	<input type="hidden" name="no_note" value="1">

	<!-- Product Information -->
	<input type="hidden" name="item_name" value="<?php echo $item_name; ?>">
	<input type="hidden" name="amount" value="<?php echo $amount; ?>">
	<input type="hidden" name="quantity" value="<?php echo $quantity; ?>">
	<input type="hidden" name="item_number" value="<?php echo $item_number; ?>">

	<!-- Customer Information -->
	<input type="hidden" name="first_name" value="<?php echo $login_user['realname']; ?>">
	<input type="hidden" name="last_name" value="">
	<input type="hidden" name="address1" value="<?php echo $login_user['address']; ?>">
	<input type="hidden" name="address2" value="">

	<input type="hidden" name="zip" value="<?php echo $login_user['zipcode']; ?>">
	<input type="hidden" name="email" value="<?php echo $login_user['email']; ?>">
	<img src="/static/css/i/paypal.png" /><br/><input type="submit" class="formbutton gotopay" value="Go to PayPal" style="vertical-align:middle;"/>
</form>
<?php } else if($order['service']=='chinabank') { ?>
<form id="order-pay-form" method="post" action="https://pay3.chinabank.com.cn/PayGate" target="_blank" sid="<?php echo $order['id']; ?>">
	<input type="hidden" name="v_mid" value="<?php echo $v_mid; ?>" />
	<input type="hidden" name="v_oid" value="<?php echo $v_oid; ?>" />
	<input type="hidden" name="v_amount" value="<?php echo $total_money; ?>" />
	<input type="hidden" name="v_moneytype" value="<?php echo $v_moneytype; ?>" />
	<input type="hidden" name="v_url" value="<?php echo $v_url; ?>" />
	<input type="hidden" name="v_md5info" value="<?php echo $v_md5info; ?>" />
	<img src="/static/css/i/chinabank.png" /><br/><input type="submit" class="formbutton gotopay" value="前往网银在线支付" style="vertical-align:middle;"/>
</form>
<?php }?>
						<div class="back-to-check"><a href="/order/check.php?id=<?php echo $order_id; ?>">&raquo; 返回选择其他支付方式</a></div>
                    </div>
                </div>
            </div>
            <div class="box-bottom"></div>
        </div>
    </div>
</div>
</div> <!-- bd end -->
</div> <!-- bdw end -->

<?php include template("footer");?>
