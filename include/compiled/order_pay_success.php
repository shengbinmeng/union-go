<?php include template("header");?>
<div id="bdw" class="bdw">
<div id="bd" class="cf">
<div id="content">
    <div id="order-pay-return" class="box">
        <div class="box-top"></div>
        <div class="box-content">
			<div class="success"><h2>您的订单，支付成功了！</h2> </div>
            <div class="sect">
                <p class="error-tip">查看所购项目&nbsp;<a href="/team.php?id=<?php echo $order['team_id']; ?>"><?php echo $team['title']; ?></a>&nbsp;的&nbsp;<a href="/order/view.php?id=<?php echo $order_id; ?>">订单详情</a>。</p>
            </div>
		</div>
		<div class="box-bottom"></div>
	</div>
</div>
<div id="side">
</div>
</div> <!-- bd end -->
</div> <!-- bdw end -->

<?php include template("footer");?>
