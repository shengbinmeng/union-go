<?php include template("header");?>

<div id="bdw" class="bdw">
<div id="bd" class="cf">
<div id="forum">
	<div class="dashboard" id="dashboard">
		<?php 
		$sql='SELECT * FROM category WHERE zone="public"';
		$result=DB::Query($sql); ?>
		<ul>
		<li class="<?php echo $id ? '':'current'; ?>"><a href="/forum/public.php">公共讨论区</a><span></span></li>
		<?php while($board=mysql_fetch_array($result)){ ?>
		<li class="<?php echo $board['id']==$id ? 'current':''; ?>"><a href="/forum/public.php?id=<?php echo $board['id']; ?>"><?php echo $board['name']; ?></a><span></span></li>
		<?php } ?>
		</ul>
	</div>
    <div id="content" class="coupons-box clear">
		<div class="box clear">
            <div class="box-top"></div>
            <div class="box-content">
                <div class="head">
                    <h2>发表新话题</h2>
				</div>
                <div class="sect">
                    <form id="forum-new-form" method="post" class="validator">
                        <div class="field">
                            <label>讨论区</label>
							<select name='category' style="margin-top:3px;">
							
							<optgroup label="公共讨论区">
								<?php echo Utility::Option($publics, $id); ?>
							</optgroup>
							</select>
                        </div>
						<div class="field">
							<label>标题</label>
							<input type="text" size="10" name="title" id="team-create-style" class="f-input" value="<?php echo htmlspecialchars($topic['title']); ?>" datatype="require" require="true" />
						</div>
						<div class="field">
							<label>内容</label>
							<textarea style="width:480px;height:240px;" name="content" id="forum-new-content" class="f-textarea" datatype="require" require="true"><?php echo htmlspecialchars($topic['content']); ?></textarea>
						</div>
						<div class="act">
                            <input type="submit" value="成立" name="commit" id="leader-submit" class="formbutton"/>
                        </div>
                    </form>
				</div>
            </div>
            <div class="box-bottom"></div>
        </div>
    </div>
    <div id="sidebar">
		<?php include template("block_side_subscribe");?>
    </div>
</div>

</div> <!-- bd end -->
</div> <!-- bdw end -->

<?php include template("footer");?>
