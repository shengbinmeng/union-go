<?php include template("header");?>

<div id="bdw" class="bdw">
<div id="bd" class="cf">
<div id="credit">
	<div class="dashboard" id="dashboard">
		<ul><?php echo current_account('/credit/score.php'); ?></ul>
	</div>
    <div id="content">
		<div class="box clear">
            <div class="box-top"></div>
            <div class="box-content">
                <div class="head">
                    <h2>积分余额</h2>
                    <ul class="filter">
						<li class="label">分类: </li>
						<?php echo current_credit_index('score'); ?>
					</ul>
                </div>
                <div class="sect">
					<h3 class="credit-title">当前的账户积分是：<strong><?php echo moneyit($login_user['score']); ?></strong></h3>
					<table id="order-list" cellspacing="0" cellpadding="0" border="0" class="coupons-table">
						<tr><th width="120">时间</th><th width="auto">详情</th><th width="50">收支</th><th width="70">积分</th></tr>
						<?php if(is_array($credits)){foreach($credits AS $index=>$one) { ?>
						<tr <?php echo $index%2?'':'class="alt"'; ?>><td style="text-align:left;"><?php echo date('Y-m-d H:i', $one['create_time']); ?></td><td><?php echo ZCredit::Explain($one); ?></td><td class="<?php echo $one['direction']; ?>"><?php echo $one['score']>0?'收入':'支出'; ?></td><td><?php echo moneyit($one['score']); ?></td></tr>
						<?php }}?>
						<tr><td colspan="4"><?php echo $pagestring; ?></td></tr>
                    </table>
				</div>
            </div>
            <div class="box-bottom"></div>
        </div>
    </div>
    <div id="sidebar">
		<?php include template("block_side_score");?>
    </div>
</div>

</div> <!-- bd end -->
</div> <!-- bdw end -->

<?php include template("footer");?>
