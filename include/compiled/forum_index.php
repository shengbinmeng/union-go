<?php include template("header");?>

<div id="bdw" class="bdw">
<div id="bd" class="cf">
<div id="forum">
	<div class="dashboard" id="dashboard">
		<ul><?php echo current_forum('index'); ?></ul>
	</div>
    <div id="content" class="coupons-box clear">
		<div class="box clear">
            <div class="box-top"></div>
            <div class="box-content">
                <div class="head">
                    <h2>所有话题</h2>
					<ul class="filter"><li><a href="/forum/new.php">＋发表新话题</a></li></ul>
				</div>
                <div class="sect">
					<table id="orders-list" cellspacing="0" cellpadding="0" border="0" class="coupons-table">
						<tr><th width="360">话题</th><th width="80" nowrap>分类</th><th width="80" nowrap>回复/阅读</th><th width="100" nowrap>最后发表</th></tr>
					<?php if(is_array($topics)){foreach($topics AS $index=>$one) { ?>
						<tr <?php echo $index%2?'':'class="alt"'; ?>><td style="text-align:left;"><a class="deal-title" href="/forum/topic.php?id=<?php echo $one['id']; ?>" style="<?php echo $one['head']?'color:#F00;':''; ?>"><?php echo htmlspecialchars($one['title']); ?></a></td><td nowrap><?php if($one['public_id']){?>公共讨论区<?php } else if($one['team_id']) { ?>团购讨论区<?php } else { ?>本地讨论区<?php }?></td><td align="center" nowrap><?php echo $one['reply_number']; ?>/<?php echo $one['view_number']; ?></td><td class="author" nowrap><?php echo $users[$one['last_user_id']]['username']; ?><br/><?php echo Utility::HumanTime($one['last_time']); ?></td></tr>
					<?php }}?>
						<tr><td colspan="4"><?php echo $pagestring; ?></td></tr>
                    </table>
				</div>
            </div>
            <div class="box-bottom"></div>
        </div>
    </div>
    <div id="sidebar">
		<?php include template("block_side_subscribe");?>
    </div>
</div>

</div> <!-- bd end -->
</div> <!-- bdw end -->

<?php include template("footer");?>
