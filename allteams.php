<?php
require_once(dirname(__FILE__) . '/app.php');
if(!$INI['db']['host']) redirect( WEB_ROOT . '/install.php' );
?>

<?php include template("header");?>
<div id="bdw" class="bdw">
<div id="bd" class="cf">
	
<?php
$request_uri = 'index';
$team = current_team($city['id']);
/* your order */
if ($login_user_id && 0==$team['close_time']) {
	$order = DB::LimitQuery('order', array(
		'condition' => array(
			'team_id' => $id,
			'user_id' => $login_user_id,
			'state' => 'unpay',
		),
		'one' => true,
	));
}
/* end order */
?>

<?php if($order){?>
<div id="sysmsg-tip" ><div class="sysmsg-tip-top"></div><div class="sysmsg-tip-content">您已经下过订单，但还没有付款。<a href="/order/check.php?id=<?php echo $order['id']; ?>">查看订单并付款</a><span id="sysmsg-tip-close" class="sysmsg-tip-close">关闭</span></div><div class="sysmsg-tip-bottom"></div></div>
<?php }?>

<div id="deal-default">

<?php
$city = Table::Fetch('category', $team['city_id']);
if(!$city) { $city = array('id' => 0, 'name' => '全部', ); }

$now = time();

$oc = array( 
			'city_id' => $city['id'], 
			//"id <> {$id}",
			"begin_time < {$now}",
			"end_time > {$now}",
			"team_type"=>"normal",
			);
$allteams = DB::LimitQuery('team', array(
			'condition' => $oc,
			'order' => 'ORDER BY sort_order DESC',
			'size' => abs(intval($INI['system']['sideteam'])),
			));
//var_dump($allteams);

if(is_array($allteams))
{
?>

	<div id="content">
<?php	
	foreach($allteams AS $team)
	{
		$city = Table::Fetch('category', $team['city_id']);
		if(!$city) { $city = array('id' => 0, 'name' => '全部', ); }

		$pagetitle = $team['title'];

		$discount_price = $team['market_price'] - $team['team_price'];
		$discount_rate = $team['team_price']/$team['market_price']*10;

		$left = array();
		$now = time();
		$diff_time = $left_time = $team['end_time']-$now;

		$left_day = floor($diff_time/86400);
		$left_time = $left_time % 86400;
		$left_hour = floor($left_time/3600);
		$left_time = $left_time % 3600;
		$left_minute = floor($left_time/60);
		$left_time = $left_time % 60;

		/* progress bar size */
		$bar_size = ceil(190*($team['now_number']/$team['min_number']));
		$bar_offset = ceil(5*($team['now_number']/$team['min_number']));

		$partner = Table::Fetch('partner', $team['partner_id']);
		$team['state'] = team_state($team);
?>	
		<?php include template("block_team_share");?>
		<div id="deal-intro" class="cf">
            <h1><?php if($team['close_time']==0){?><a class="deal-today-link" href="/team.php?id=<?php echo $team['id']; ?>">今日团购：</a><?php }?> <a href="/team.php?id=<?php echo $team['id']; ?>"> <?php echo $team['title']; ?></a> </h1>
            <div class="main">
                <div class="deal-buy">
                    <div class="deal-price-tag"></div>
				<?php if(($team['state']=='soldout')){?>
                    <p class="deal-price"><strong><?php echo $currency; ?><?php echo moneyit($team['team_price']); ?></strong><span class="deal-price-soldout"></span></p>
				<?php } else if($team['close_time']) { ?>
                    <p class="deal-price"><strong><?php echo $currency; ?><?php echo moneyit($team['team_price']); ?></strong><span class="deal-price-expire"></span></p>
				<?php } else { ?>
                    <p class="deal-price"><strong><?php echo $currency; ?><?php echo moneyit($team['team_price']); ?></strong><span><a <?php echo $team['begin_time']<time()?'href="/team/buy.php?id='.$team['id'].'"':''; ?>><img src="/static/css/i/button-deal-buy.gif" /></a></span></p>
				<?php }?>
                </div>

                <table class="deal-discount">
<!--
                    <tr>
                        <th>原价</th>
                        <th>折扣</th>
                        <th>节省</th>
                    </tr>
                    <tr>
                        <td><?php echo $currency; ?><?php echo moneyit($team['market_price']); ?></td>
					<?php if(($team['market_price']>0&&$team['team_price']>0)){?>
                        <td><?php echo moneyit($discount_rate); ?>折</td>
					<?php } else { ?>
						<td>?</td>
					<?php }?>
                        <td><?php echo $currency; ?><?php echo $discount_price; ?></td>
                    </tr>
-->
                </table>

				<?php if($team['close_time']){?>
                <div class="deal-box deal-timeleft deal-off" id="deal-timeleft" curtime="<?php echo $now; ?>000" diff="<?php echo $diff_time; ?>000">
					<h3>本团购结束于</h3>
					<div class="limitdate"><p class="deal-buy-ended"><?php echo date('Y-m-d', $team['close_time']); ?><br><?php echo date('H:i:s', $team['close_time']); ?></p></div>
				</div>
				<?php } else { ?>
                <div class="deal-box deal-timeleft deal-on" id="deal-timeleft" curtime="<?php echo time(); ?>000" diff="<?php echo $diff_time; ?>000">
					<h3>剩余时间</h3>
					<div class="limitdate"><ul id="<?php echo 'counter'.$team['id']; ?>" >
					<?php if($left_day>0){?>
						<li><span><?php echo $left_day; ?></span>天</li><li><span><?php echo $left_hour; ?></span>小时</li><li><span><?php echo $left_minute; ?></span>分钟</li>
					<?php } else { ?>
						<li><span><?php echo $left_hour; ?></span>小时</li><li><span><?php echo $left_minute; ?></span>分钟</li><li><span><?php echo $left_time; ?></span>秒</li>
					<?php }?>
					</ul></div>
				</div>
				<?php }?>

			<?php if($team['close_time']==0){?>
				<?php if($team['state']=='none'){?>
				<div class="deal-box deal-status" id="deal-status">
					<p class="deal-buy-tip-top"><strong><?php echo $team['now_number']; ?></strong> 人已购买</p>
					<div class="progress-pointer" style="padding-left:<?php echo $bar_size-$bar_offset; ?>px;"><span></span></div>
					<div class="progress-bar"><div class="progress-left" style="width:<?php echo $bar_size-$bar_offset; ?>px;"></div><div class="progress-right "></div></div>
					<div class="cf"><div class="min">0</div><div class="max"><?php echo $team['min_number']; ?></div></div>
					<p class="deal-buy-tip-btm">还差 <strong><?php echo $team['min_number']-$team['now_number']; ?></strong> 人到达最低团购人数</p>
				</div>
				<?php } else { ?>
				<div class="deal-box deal-status deal-status-open" id="deal-status">
					<p class="deal-buy-tip-top"><strong><?php echo $team['now_number']; ?></strong> 人已购买</p>
					<p class="deal-buy-on" style="line-height:200%;"><img src="/static/css/i/deal-buy-succ.gif"/> 团购成功！ <?php if($team['max_number']>$team['now_number']||$team['max_number']==0){?><br/>还可以继续购买<?php }?></p>
				</div>
				<?php }?>
			<?php } else { ?>
				<div class="deal-box deal-status deal-status-<?php echo $team['state']; ?>" id="deal-status"><div class="deal-buy-<?php echo $team['state']; ?>"></div><p class="deal-buy-tip-total">共有 <strong><?php echo $team['now_number']; ?></strong> 人购买</p></div> 
			<?php }?>
			</div>
            <div class="side">
                <div class="deal-buy-cover-img" id="team_images">
				<?php if($team['image1']||$team['image2']){?>
					<div class="mid">
						<ul>
							<li class="first"><img src="<?php echo team_image($team['image']); ?>"/></li>
						<?php if($team['image1']){?>
							<li><img src="<?php echo team_image($team['image1']); ?>"/></li>
						<?php }?>
						<?php if($team['image2']){?>
							<li><img src="<?php echo team_image($team['image2']); ?>"/></li>
						<?php }?>
						</ul>
						<div id="img_list">
							<a ref="1" class="active">1</a>
						<?php $imageindex=1;; ?>
						<?php if($team['image1']){?>
							<a ref="<?php echo ++$imageindex; ?>" ><?php echo $imageindex; ?></a>
						<?php }?>
						<?php if($team['image2']){?>
							<a ref="<?php echo ++$imageindex; ?>" ><?php echo $imageindex; ?></a>
						<?php }?>
						</div> 
					</div>
					<?php } else { ?>
						<img src="<?php echo team_image($team['image']); ?>"/>
					<?php }?>
				</div>
                <div class="digest"><br /><?php echo nl2br(strip_tags($team['summary'])); ?></div>
            </div>

		</div>
		<br />
<?php			
	}
}
?>

	</div><!-- content end -->
	
	<div id="sidebar">
	<br /><img src="intro.gif" ><br /><br />
	<?php include template("block_side_invite");?>
	<?php //include template("block_side_bulletin");?>
	<?php include template("block_side_flv");?>
	<?php //include template("block_side_ask");?>
	<?php include template("block_side_others");?>
	<?php include template("block_side_business");?>
	<?php include template("block_side_vote");?>
	<?php include template("block_side_subscribe");?>
	</div>
 	</div>
</div> <!-- bd end -->
</div> <!-- bdw end -->

<?php include template("footer");?>
