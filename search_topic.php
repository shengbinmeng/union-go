<?php include template("header");?>

<div id="bdw" class="bdw">
<div id="bd" class="cf">
<div id="forum">
    <div id="content" class="coupons-box clear">
		<div class="box clear">
            <div class="box-top"></div>
            <div class="box-content">
                <div class="head">
                    <h2><?php echo $pagetitle; ?></h2>
					<ul class="filter"><li><a href="/forum/new.php<?php if($public){?>?id=<?php echo $public['id']; ?><?php }?>">＋发表新话题</a></li></ul>
				</div>
                <div class="sect">
					<table id="orders-list" cellspacing="0" cellpadding="0" border="0" class="coupons-table">
						<tr><th width="360">话题</th><th width="80" nowrap>讨论区</th><th width="120" nowrap>支持/回复/阅读</th><th width="100">最后发表</th></tr>
					<?php if(is_array($topics)){foreach($topics AS $index=>$one) { ?>
						<tr <?php echo $index%2?'':'class="alt"'; ?>>
							<td style="text-align:left;"><a class="deal-title" href="/forum/topic.php?id=<?php echo $one['id']; ?>" style="<?php echo $one['head']?'color:#F00;':''; ?>"><?php echo htmlspecialchars($one['title']); ?></a></td>
							<td nowrap><a href="/forum/public.php?id=<?php echo $one['public_id']; ?>"><?php echo $publics[$one['public_id']]; ?></a></td>
							<td align="center" nowrap><?php $condition=array('team_id'=>0,'topic_id'=>$one['id']); $count = Table::Count('recom_vote', $condition); echo $count;?>/<?php echo $one['reply_number']; ?>/<?php echo $one['view_number']; ?></td>
							<td class="author" nowrap><?php echo $users[$one['last_user_id']]['username']; ?><br/><?php echo Utility::HumanTime($one['last_time']); ?></td>
						</tr>
					<?php }}?>
						<tr><td colspan="4"><?php echo $pagestring; ?></td></tr>
                    </table>
				</div>
            </div>
            <div class="box-bottom"></div>
        </div>
    </div>
    <div id="sidebar">
    	<?php include template("block_side_vote");?>
		<?php include template("block_side_subscribe");?>
    </div>
</div>

</div> <!-- bd end -->
</div> <!-- bdw end -->

<?php include template("footer");?>
