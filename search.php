<?php
require_once(dirname(__FILE__) . '/app.php');

if(!empty($_GET['words']) && strcmp($_GET['words'],"输入搜索内容...")!=0)
{
	$pwords=$_GET['words'];
	$conditions=" and title like '%".$pwords."%' ";
	$pagetitle='"'.$pwords.'"的搜索结果';
}


if(intval($_GET['type'])==0)
{
	$pstr="and team_type='normal'";
	$type="热销商品：";
}
elseif (intval($_GET['type'])==1)
{
	$pstr="and team_type='seconds'";
	$type="投票商品：";
}
elseif (intval($_GET['type'])==2)
{
	$type="全部商品：";
}
elseif (intval($_GET['type'])==3)
{
	$type="论坛话题：";
	$pagetitle=$type.$pagetitle;
	
	$conditions .="and parent_id=0";
	$perpage=8;
	$pagex = intval($_GET['page']);
	if($pagex==0) $pagex+=1;
	$sql="SELECT COUNT(1) AS count FROM topic WHERE 1 = 1 ".$conditions;
	$row=DB::GetQueryResult($sql,true);
	$totalnum=$row['count'];
	list($pagesize, $offset, $pagestring) = pagestring($totalnum, $perpage);
	$sql="select * from topic where 1 = 1 ".$conditions." limit ".($pagex-1)*$perpage.", ".$perpage;
	$topics=DB::GetQueryResult($sql,false);
	
	$user_ids = Utility::GetColumn($topics, 'user_id');
	$luser_ids = Utility::GetColumn($topics, 'last_user_id');
	$user_ids = array_merge($user_ids, $luser_ids);
	$users = Table::Fetch('user', $user_ids);
	
	die(require("search_topic.php"));
}

$conditions .=$pstr;
$pagetitle=$type.$pagetitle;

$perpage=8;
$pagex = intval($_GET['page']);
if($pagex==0) $pagex+=1;
$sql="SELECT COUNT(1) AS count FROM team WHERE 1 = 1 ".$conditions;
$row=DB::GetQueryResult($sql,true);
$totalnum=$row['count'];
list($pagesize, $offset, $pagestring) = pagestring($totalnum, $perpage);
$sql="select * from team where 1 = 1 ".$conditions." limit ".($pagex-1)*$perpage.", ".$perpage;
$teams=DB::GetQueryResult($sql,false);

require("search_view.php");  //
?>