<?php
require_once('app.php');

$daytime = strtotime(date('Y-m-d'));
$condition = array(
	'team_type' => 'normal',
	'city_id' => array(0, abs(intval($city['id']))),
	"begin_time <= '{$daytime}'",
);

$group_id = abs(intval($_GET['gid']));
if ($group_id) $condition['group_id'] = $group_id;

$count = Table::Count('team', $condition);
list($pagesize, $offset, $pagestring) = pagestring($count, 10);
$teams = DB::LimitQuery('team', array(
	'condition' => $condition,
	'order' => 'ORDER BY begin_time DESC, sort_order DESC, id DESC',
	'size' => $pagesize,
	'offset' => $offset,
));
foreach($teams AS $id=>$one){
	team_state($one);
	if (!$one['close_time']) $one['picclass'] = 'isopen';
	if ($one['state']=='soldout') $one['picclass'] = 'soldout';
	$teams[$id] = $one;
}

$category = Table::Fetch('category', $group_id);
$pagetitle = '当前联购商品';

?>

<?php include template("header");?>

<div id="bdw" class="bdw">
<div id="bd" class="cf">
<div id="recent-deals">
<?php if(option_yes('cateteam')){?>
	<div class="dashboard" id="dashboard">
		<ul><?php echo current_teamcategory($group_id); ?></ul>
	</div>
<?php }?>
    <div id="content">
        <div class="box">
            <div class="box-top"></div>
            <div class="box-content">
                <div class="head"><h2><?php echo $pagetitle; ?> <?php if($category['name']) echo " - {$category['name']}类"; ?></h2></div>
				<div class="sect">
					<?php if(is_array($teams)){ ?>
						<table border="1" bordercolor="#FFFFFF" width="100%">
						<?php foreach($teams AS $index=>$one) {
							 $partner = Table::Fetch('partner', $one['partner_id']); ?>
						<tr bgcolor="#FFFFFF">
							<td align="center">
							<h4><?php echo mb_strimwidth($one['title'],0,86,'...'); ?></h4>
							<div>
								<img alt="<?php echo $one['title']; ?>" src="<?php echo team_image($one['image'], true); ?>" width="120" height="80">
							</div>
							</td>
							<td>
							<div>
								<p class="total"><strong class="count"><?php echo $one['now_number']; ?></strong>人购买</p>
								<p class="price"><font color="blue">原价：<strong class="old"><span class="money"><?php echo $currency; ?></span><?php echo moneyit($one['market_price']); ?></strong><br />折扣：<strong class="discount"><?php echo team_discount($one); ?>折</strong><br />现价：<strong><span class="money"><?php echo $currency; ?></span><?php echo moneyit($one['team_price']); ?></strong><br />节省：<strong><span class="money"><?php echo $currency; ?></span><?php echo moneyit($one['market_price']-$one['team_price']); ?></strong><br /></p>
							<?php if(option_yes('moneysave')){?><p>共为用户节省:<strong class="count"><?php echo $currency; ?><?php echo moneyit($one['now_number']*($one['market_price']-$one['team_price'])); ?></strong></p><?php }?>
							</div>
							</td>
							<td align="center"><font color="red"><b>
								<?php echo $one['summary']; ?>
							</td>
							<td align="center"><font size="2">
								<?php if($one['detail']) echo $one['detail']; else echo '无详细介绍';?>
							</td>
							<td align="center"><font size="2">
								<?php echo $partner['homepage'] ? $partner['homepage'] : "无"; //客服?>
							</td>
							<td align="center"><font size="2">
								<?php echo $partner['location'] ? $partner['location'] : "无"; //电话?>
							</td>
							<td align="center"><font size="2">
								<?php echo $partner['other'] ? $partner['other'] : "无"; //承诺?>
							</td>
							<td align="center">
								<a href="/team.php?id=<?php echo $one['id']; ?>" title="<?php echo $one['title']; ?>" target="_blank"><font color="blue">去购买</a>
							</td>
						</tr>
						<tr><td colspan="8"><hr></td></tr>
						<?php } ?>
</table>
					<?php	}?>
					<div class="clear"></div>
					<div><?php echo $pagestring; ?></div>
				</div>
            </div>
            <div class="box-bottom"></div>
        </div>
    </div>
	<div id="sidebar">
		<?php include template("block_side_vote");?>
		<?php include template("block_side_subscribe");?>
	</div>
</div>
    </div> <!-- bd end -->
</div> <!-- bdw end -->

<?php include template("footer");?>



<?php 
function current_teamcategory($gid='0') {
	global $city;
	$a = array(
			'/allteams.php' => '所有',
			);
	foreach(option_hotcategory('group') AS $id=>$name) {
		$a["/allteams.php?gid={$id}"] = $name;
	}
	$l = "/allteams.php?gid={$gid}";
	if (!$gid) $l = "/allteams.php";
	return current_link($l, $a, true);
}
?>
